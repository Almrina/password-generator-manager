import random
import docx

doc = docx.Document("C:\\Users\karin\\source\\repos\\Password Gen-Manager\\PM.docx")

Big = 'QWERTYUIOPASDFGHJKLZXCVBNM'
Low = 'qwertyuiopasdfghjklzxcvbnm'
Num = '1234567890'
Spe = '!@#$%^&amp;*()'
 
BI = False  # Password must contain upper case characters? (True - yes, False - no)
LO = True  # Password must contain lowercase characters? (True - yes, False - no)
NU = True  # Password must contain numbers? (True - yes, False - no)
PS = True  # Password must contain special characters? (True - yes, False - no)

webs = input("What website do you need a password for? ")
usrnm = input("What is your username for this website? ")
Password_len = input('Password length: ')
 
if Password_len:
   if Password_len.isdigit() == True:
       Password_len = int(Password_len)
   else:
      print('The program has ended. The value must be numeric.')
      exit(0)
else:
   print('The program has ended. Password length not specified.')
   exit(0)
 
Password_cou = input('Number of passwords: ')
print('\n')
 
if Password_cou:
   if Password_cou.isdigit() == True:
       Password_cou = int(Password_cou)
   else:
      print('The program has ended. The value must be numeric.')
      exit(0)
else:
   print('The program has ended. The number of passwords is not specified.')
   exit(0)
 
Pass_Symbol = []
if BI == True:
   Pass_Symbol.extend(list(Big))
 
if LO == True:
   Pass_Symbol.extend(list(Low))
 
if NU == True:
   Pass_Symbol.extend(list(Num))
 
if PS == True:
   Pass_Symbol.extend(list(Spe))
 
random.shuffle(Pass_Symbol)
psw = []
 
for yx in range(Password_cou):
   psw.append(''.join([random.choice(Pass_Symbol) for x in range(Password_len)]))


doc.add_paragraph("Website: ")
doc.add_paragraph(webs)
doc.add_paragraph("Username: ")
doc.add_paragraph(usrnm)
doc.add_paragraph("Password: ")
doc.add_paragraph(psw)
doc.add_paragraph("\n")

doc.save("C:\\Users\karin\\source\\repos\\Password Gen-Manager\\PM.docx")

print('\n'.join(psw))
print('\n')
