# Password Generator + Manager

A password manager is software that helps the user manage passwords and PINs. The password manager has a built-in password generator that will allow you to create new passwords according to the specified criteria. This password manager helps you create strong unique passwords for websites and stores them in a document.

## Programming language

Python

## Usage

![Image alt](https://gitlab.com/Almrina/password-generator-manager/raw/main/Usage.gif)

## Contributing

I am open to contributions. Please contact me in order to if you want to make changes to the project or use it in your own form.

When creating the project, two libraries were used (random and docx, which requires installation via ```pip install docx```). The created environment env was used. The docx library was installed in it via PowerShell.

## Project status

The development of the project at this stage is completed and slowed down for development.

## License

MIT License

Copyright (c) [2022] [Litvinova Karina Vladimirovna]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
